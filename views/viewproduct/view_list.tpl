<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body style="background: #f1f2f3">
        <input type="hidden" value="{$Query.searchkey}" id="searchkey" />
        <input type="hidden" value="{$cat}" id="cat" />
        <input type="hidden" value="{$orderby}" id="orderby" />
        <div class="listTopcaption">
            <div class="listTopArrow" data-swclass="arraw-ani" data-link="javascript:history.go(-1);"></div>
            <a class="listTopArrow home" href="javascript:;" onclick="location = '?/';"></a>
            {$catInfo['cat_name']}
        </div>
        {include file="../global/subnav1.tpl"}
        {include file="../global/search_box1.tpl"}
        <div id="product_list" class="clearfix"></div>
        <div id="loading-wrap"></div>
        <script type="text/javascript">
            WeixinJSBridgeReady(pvListOnload);
        </script>
        {include file="../global/footer.tpl"}
    </body>
</html>