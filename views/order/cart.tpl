<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="{$docroot}static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="{$docroot}static/css/common.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="{$docroot}static/script/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/main.js?v={$cssversion}"></script>        
        <script type="text/javascript" src="{$docroot}static/script/crypto-sha1.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/crypto-md5.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/cart.js?v={$cssversion}"></script>
    </head>
    <body>

        <input type="hidden" id="paycallurl" value="{$docroot}?/Order/ajaxGetBizPackage" />
        <input type="hidden" id="paycallorderurl" value="{$docroot}?/Order/ajaxCreateOrder" />

        <div class="listTopcaption">
            <a class="listTopArrow" href="javascript:;" onclick="history.go(-1);"></a>
            <a class="listTopArrow home" href="javascript:;" onclick="location = './';"></a>
            购物车
        </div>

        <div id="cat-orderlist">收货信息</div>

        <a id="express_address" href="javascript:addAddress();">
            <div id="wrp-btn">点击选择收货地址</div>
            <div class="express-person-info clearfix">
                <div class="express-person-name">
                    <span id="express-name"></span><span id="express-person-phone"></span>
                </div>
            </div>
            <div class="express-address-info">
                <span id="express-address"></span>
            </div>
        </a>

        <div id="cat-orderlist">订单信息</div>

        <div id="orderDetailsWrapper"></div>

        <div id="orderSummay">
            <div>
                <input type="checkbox" id="cart-balance-check"/> 使用余额 <b id="cart-balance-pay">{$userInfo['balance']}</b>
            </div>
            <div>
                总计：<b class="prices" id="order_amount">&yen;0</b>
            </div>
        </div>
            
        <a class="button green" id="wechat-payment-btn" href="javascript:;" onclick="wepayCall()">微信安全支付</a>
        <script type="text/javascript">
            WeixinJSBridgeReady(loadCartData);
        </script>
        <script type="text/javascript">
            function addAddress()
            {
                WeixinJSBridge.invoke('editAddress', {
                    "appId": "{$appid}",
                    "scope": "jsapi_address",
                    "signType": "sha1",
                    "addrSign": "{$addrsign}",
                    "timeStamp": "{$timestamp}",
                    "nonceStr": "{$nonceStr}"
                }, addAddressCallback);
            }
        </script>
        {include file="../global/footer.tpl"}
    </body>
</html>