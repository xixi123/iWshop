/* 
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

var is_adminhome = false;

var orderListloaded = false;

var defaultCard = 1;

// jq lis
$(function() {
    if ($('.wdmin-main').length > 0) {
        is_adminhome = true;
        // load card
        switchCard(defaultCard);
    }

    // qrcodeScan login
    if (!is_adminhome) {
        qrcodeScanListen();
    }

    $('#despatchBtn').click(despatch);

    // resize
    window.onresize = __resize__;
    __resize__();
});

/**
 * window resizing lis
 * @returns {undefined}
 */
function __resize__() {
    $('.wdmin-login').height(document.documentElement.clientHeight + 'px');
    $('.wdmin-main,#rightWrapper').height(document.documentElement.clientHeight - 70 + 'px');
    $('#main-mid,#main-mid-left,#main-mid-right').height(document.documentElement.clientHeight - 60 - 70 + 'px');
    $('#home-orderlist').height(document.documentElement.clientHeight - 60 - 70 - 220 + 'px');
    absmid('#qrcode-login-wrapp');
}

/**
 * qrcodeScan lis thread
 * @returns {undefined}
 */
function qrcodeScanListen() {
    var qco = $('#qrcode-co').val();
    var req = '?/Wdmin/checkLogin/?qco=' + qco;
    setTimeout(function() {
        function check() {
            $.get(req, function(r) {
                if (r !== '0') {
                    r = r.toJson();
                    login(r.loginKey);
                } else {
                    // 500ms check again
                    setTimeout(check, 500);
                }
            });
        }
        check();
    }, 2000);
}

function login(loginKey) {
    $.cookie('loginKey', loginKey);
    window.location.href = '?/Wdmin';
}

/**
 * switchCard
 * @param {type} cardId
 * @returns {undefined}
 */
function switchCard(cardId) {
    $('._card').removeClass('_show');
    $('#card' + cardId).addClass('_show');

    switch (cardId) {
        case 1:
            ajaxLoadPageViewChart();
            ajaxLoadSaleViewChart();
            break;
        case 2:
            // order list
            ajaxLoadOrderlist();
            break;
    }

    loadStatData();
}

/**
 * ajaxLoadOrderlist
 * @returns {undefined}
 */
function ajaxLoadOrderlist() {
    Loading.start("#main-mid");
    $.get('?/Wdmin/ajaxLoadOrderlist/page=0', function(r) {
        Loading.finish();
        $('#orderlist').html(r);
        $(".various").fancybox({
            openEffect: 'fade',
            closeEffect: 'elastic',
            openSpeed: 'fast',
            closeSpeed: 'fast',
            afterLoad: function(r) {
                window.corder1 = parseInt($(r.element[0]).attr('data-orderid'));
                $('#_orderwpa').html($('#orderWpa' + corder1).html());
            }
        });
    });
}

/**
 * despatch goods
 * @params window.corder1 选中orderid
 * @returns {undefined}
 */
function despatch() {
    var orderId = window.corder1;
    var despatchExpressCode = $('#despatchExpressCode').val();
    var expressCompany = $('#expressCompany').val();

    if (despatchExpressCode === "") {
        $('#despatchExpressCode').addClass('shake').css('border-color', '#900');
        setTimeout(function() {
            $('#despatchExpressCode').removeClass('shake');
        }, 500);
    } else {
        // 发货走起
        $.post('?/Order/ExpressReady', {
            'orderId': orderId,
            'ExpressCode': despatchExpressCode,
            'expressCompany': expressCompany
        }, function(res) {
            if (res === "1") {
                alert('发货成功');
                $.fancybox.close();
                $('#orderlst' + orderId).slideUp();
            } else {
                alert('failed');
            }
        });
    }
}

/**
 * loadStatData
 * @returns {undefined}
 */
function loadStatData() {
    $.get('?/Wdmin/ajaxGetOrderCount', function(r) {
        $('#stat-ordercount').html(r);
    });
}

function ajaxLoadPageViewChart() {
    $.get('?/Wdmin/ajaxGetPageViewData', function(res) {
        res = res.toJson();
        for(var k in res.x){
            res.x[k] = k + '点';
        }
        $('#daytot').html(res.daytot);
        $('#montot').html(res.montot);
        $('#pageviewchart').highcharts({
            title: {
                text: '微店浏览量', x: 0
            },
            chart: {
                type: 'line'
            },
            xAxis: {
                categories: res.x,
                lineWidth: 0
            },
            yAxis: {
                title: {
                    text: ''
                },
                minPadding: 0
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: '浏览量',
                    data: res.y
                }]
        });
    });
}

function ajaxLoadSaleViewChart() {
    $.get('?/Wdmin/ajaxLoadSaleHisCartData', function(res) {
        res = res.toJson();
        console.log(res);
        $('#saletReachart').highcharts({
            title: {
                text: '本月销售趋势', x: 0
            },
            chart: {
                type: 'line'
            },
            xAxis: {
                categories: res.x,
                lineWidth: 0
            },
            yAxis: {
                title: {
                    text: ''
                },
                minPadding: 0
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: '销售额',
                    data: res.y
                }]
        });
    });
}