/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : wshop

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2014-07-10 11:50:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_login_code_token
-- ----------------------------
DROP TABLE IF EXISTS `admin_login_code_token`;
CREATE TABLE `admin_login_code_token` (
  `tid` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_bin NOT NULL,
  `createtime` datetime DEFAULT NULL,
  `bind` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `used` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`tid`,`ip`),
  UNIQUE KEY `uni` (`ip`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client_id` int(25) NOT NULL AUTO_INCREMENT COMMENT '会员卡号',
  `client_password` varchar(60) COLLATE utf8_bin DEFAULT '' COMMENT '会员密码',
  `client_type` tinyint(2) DEFAULT '1' COMMENT '会员种类\r\n1为普通会员\r\n0为合作商',
  `client_name` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '会员姓名',
  `client_sex` enum('m','f') COLLATE utf8_bin DEFAULT NULL COMMENT '会员性别',
  `client_phone` varchar(20) CHARACTER SET utf8 DEFAULT '' COMMENT '会员电话',
  `client_wechat_openid` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '会员微信openid',
  `client_joindate` date NOT NULL COMMENT '入会日期',
  `client_birth` date NOT NULL DEFAULT '1900-01-01' COMMENT '会员生日',
  `client_address` varchar(60) CHARACTER SET utf8 DEFAULT '' COMMENT '会员住址',
  `client_money` float(15,2) NOT NULL DEFAULT '0.00' COMMENT '会员存款',
  `client_credit` int(15) NOT NULL DEFAULT '0' COMMENT '会员积分',
  `client_introducer` int(25) DEFAULT '100096' COMMENT '会员介绍人',
  `client_remark` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '会员备注',
  `client_storeid` int(10) DEFAULT '0' COMMENT '会员所属店号',
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `VIP_id` (`client_id`) USING BTREE,
  UNIQUE KEY `wechat_openid` (`client_wechat_openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100265 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for client_credit_record
-- ----------------------------
DROP TABLE IF EXISTS `client_credit_record`;
CREATE TABLE `client_credit_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_infos
-- ----------------------------
DROP TABLE IF EXISTS `client_infos`;
CREATE TABLE `client_infos` (
  `openid` varchar(255) COLLATE utf8_bin NOT NULL,
  `client_id` int(11) NOT NULL,
  `headimg` varchar(512) COLLATE utf8_bin NOT NULL,
  `dir_introducer` int(11) NOT NULL DEFAULT '0',
  `top_introducer` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`client_id`,`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for client_money_record
-- ----------------------------
DROP TABLE IF EXISTS `client_money_record`;
CREATE TABLE `client_money_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `time` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_token
-- ----------------------------
DROP TABLE IF EXISTS `client_token`;
CREATE TABLE `client_token` (
  `client_id` int(11) DEFAULT NULL,
  `token` varchar(550) COLLATE utf8_bin NOT NULL,
  `token_time` varchar(20) COLLATE utf8_bin NOT NULL,
  `refresh_token` varchar(550) COLLATE utf8_bin NOT NULL,
  `openid` varchar(30) COLLATE utf8_bin NOT NULL,
  `qrscene` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`openid`),
  KEY `token` (`token`(255)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_income_record
-- ----------------------------
DROP TABLE IF EXISTS `company_income_record`;
CREATE TABLE `company_income_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float(11,2) NOT NULL DEFAULT '0.00',
  `date` datetime NOT NULL,
  `order_id` int(11) NOT NULL,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `pcount` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_spread_record
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record`;
CREATE TABLE `company_spread_record` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `readi` int(11) NOT NULL DEFAULT '0',
  `turned` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_spread_record_details
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record_details`;
CREATE TABLE `company_spread_record_details` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `spread_id` int(11) NOT NULL,
  `cclient_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for csv
-- ----------------------------
DROP TABLE IF EXISTS `csv`;
CREATE TABLE `csv` (
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `pic` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `prices` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `wei` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for introducer_discount
-- ----------------------------
DROP TABLE IF EXISTS `introducer_discount`;
CREATE TABLE `introducer_discount` (
  `intro_id` int(11) NOT NULL,
  `intro_discount` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`intro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `logcont` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `client_id` int(20) DEFAULT NULL COMMENT '客户编号',
  `order_time` datetime DEFAULT NULL COMMENT '订单交易时间',
  `order_amount` float(10,2) DEFAULT '0.00' COMMENT '总价',
  `company_com` varchar(255) COLLATE utf8_bin DEFAULT '0',
  `product_count` int(11) DEFAULT '0',
  `order_dixian` float(10,2) NOT NULL DEFAULT '0.00',
  `pay_method` enum('bankcard','vipcard','cash') COLLATE utf8_bin NOT NULL DEFAULT 'cash',
  `serial_number` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `wepay_serial` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `wepay_openid` varchar(255) COLLATE utf8_bin DEFAULT '',
  `bank_billno` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `leword` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `status` enum('unpay','payed','received','canceled','delivering') COLLATE utf8_bin NOT NULL DEFAULT 'unpay' COMMENT '订单状态',
  `express_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `express_com` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `staff_id` int(20) DEFAULT NULL COMMENT '职员工号',
  `store_id` int(10) NOT NULL DEFAULT '0' COMMENT '商店编号',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=415 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_address
-- ----------------------------
DROP TABLE IF EXISTS `orders_address`;
CREATE TABLE `orders_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel_number` varchar(255) COLLATE utf8_bin NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail` (
  `order_id` int(20) NOT NULL COMMENT '订单编号',
  `product_id` int(20) NOT NULL COMMENT '商品编号',
  `product_inprice` float(10,2) NOT NULL COMMENT '商品原价',
  `product_count` int(10) NOT NULL COMMENT '商品数量',
  `product_saleprice` float(10,2) NOT NULL COMMENT '商品售价',
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_discount_price` float(10,2) NOT NULL,
  `is_returned` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pageview_records
-- ----------------------------
DROP TABLE IF EXISTS `pageview_records`;
CREATE TABLE `pageview_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `openid` varchar(255) COLLATE utf8_bin DEFAULT '',
  `ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1479 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for products_info
-- ----------------------------
DROP TABLE IF EXISTS `products_info`;
CREATE TABLE `products_info` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `product_code` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '商品条码',
  `product_type` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品类型',
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `product_color` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品颜色',
  `product_size` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品大小',
  `product_cat` int(11) NOT NULL DEFAULT '1',
  `product_readi` int(11) NOT NULL DEFAULT '0',
  `product_start` date DEFAULT NULL,
  `product_desc` text COLLATE utf8_bin,
  `product_subtitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product_weight` float(5,2) DEFAULT NULL,
  `store_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7879 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_adjust_details
-- ----------------------------
DROP TABLE IF EXISTS `product_adjust_details`;
CREATE TABLE `product_adjust_details` (
  `record_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品批次',
  `product_id` int(20) DEFAULT NULL COMMENT '商品编号',
  `cost_price` float(10,2) DEFAULT NULL COMMENT '商品进货价',
  `product_count` int(10) DEFAULT NULL COMMENT '商品数量',
  `recode_date` datetime DEFAULT NULL COMMENT '进货日期',
  `store_id` int(10) DEFAULT NULL COMMENT '商店编号',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=763 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_adjust_record
-- ----------------------------
DROP TABLE IF EXISTS `product_adjust_record`;
CREATE TABLE `product_adjust_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_store_id` int(11) NOT NULL,
  `to_store_id` int(11) NOT NULL,
  `time` datetime DEFAULT NULL,
  `staff_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cat_parent` int(11) NOT NULL DEFAULT '0',
  `cat_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_images
-- ----------------------------
DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(512) COLLATE utf8_bin NOT NULL,
  `image_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1542 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_in_record_detail
-- ----------------------------
DROP TABLE IF EXISTS `product_in_record_detail`;
CREATE TABLE `product_in_record_detail` (
  `record_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品批次',
  `in_record_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(20) DEFAULT NULL COMMENT '商品编号',
  `cost_price` float(10,2) DEFAULT NULL COMMENT '商品进货价',
  `product_count` int(10) DEFAULT NULL COMMENT '商品数量',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_in_rercord
-- ----------------------------
DROP TABLE IF EXISTS `product_in_rercord`;
CREATE TABLE `product_in_rercord` (
  `record_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品批次',
  `cost_price_total` float(10,2) DEFAULT NULL COMMENT '商品进货价',
  `product_count_total` int(10) DEFAULT NULL COMMENT '商品数量',
  `indate` datetime DEFAULT NULL COMMENT '进货日期',
  `store_id` int(10) DEFAULT NULL COMMENT '商店编号',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_in_stock
-- ----------------------------
DROP TABLE IF EXISTS `product_in_stock`;
CREATE TABLE `product_in_stock` (
  `product_id` int(20) NOT NULL COMMENT '商品编号',
  `stock_count` int(10) NOT NULL DEFAULT '0' COMMENT '库存数量',
  `cost_price` float(10,2) NOT NULL COMMENT '商品进货价',
  `store_id` int(10) NOT NULL COMMENT '商店编号',
  PRIMARY KEY (`product_id`,`cost_price`),
  KEY `stock_count_index` (`stock_count`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_onsale
-- ----------------------------
DROP TABLE IF EXISTS `product_onsale`;
CREATE TABLE `product_onsale` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `sale_prices` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `store_id` int(8) NOT NULL DEFAULT '0' COMMENT '商店编号',
  `discount` int(3) NOT NULL DEFAULT '100' COMMENT '折扣',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7879 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_stock_change_record
-- ----------------------------
DROP TABLE IF EXISTS `product_stock_change_record`;
CREATE TABLE `product_stock_change_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `change_amount` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `record_time` datetime NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_stock_record
-- ----------------------------
DROP TABLE IF EXISTS `product_stock_record`;
CREATE TABLE `product_stock_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_count` int(11) NOT NULL,
  `stock_in_prices` float(10,2) NOT NULL,
  `stock_out_prices` float(10,2) NOT NULL,
  `record_date` date NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for shops
-- ----------------------------
DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops` (
  `store_id` int(10) NOT NULL COMMENT '商店编号',
  `store_name` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '商店名称',
  `manager_id` int(25) NOT NULL COMMENT '店长编号',
  `store_address` varchar(255) COLLATE utf8_bin DEFAULT '',
  `store_phone` varchar(255) CHARACTER SET latin1 DEFAULT '0',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for shops_settings
-- ----------------------------
DROP TABLE IF EXISTS `shops_settings`;
CREATE TABLE `shops_settings` (
  `shop_id` int(11) NOT NULL,
  `setting_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `setting_value` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for staffs
-- ----------------------------
DROP TABLE IF EXISTS `staffs`;
CREATE TABLE `staffs` (
  `staff_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '职员编号',
  `staff_password` varchar(256) CHARACTER SET utf8 NOT NULL COMMENT '职员登录密码',
  `staff_name` varchar(40) CHARACTER SET utf8 NOT NULL COMMENT '职员姓名',
  `store_id` int(10) NOT NULL COMMENT '商店编号',
  `staff_phone` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '职员电话',
  `staff_join_date` date NOT NULL COMMENT '职员入职时间',
  `staff_identity` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '职员身份证',
  `staff_position` varchar(40) CHARACTER SET utf8 NOT NULL COMMENT '职员职位',
  `staff_sex` enum('m','f') CHARACTER SET utf8 NOT NULL COMMENT '职员性别',
  `staff_pay` float(10,2) DEFAULT NULL COMMENT '职员基本工资',
  `staff_type` int(2) NOT NULL DEFAULT '1' COMMENT '员工分类\r\n0：管理员\r\n1：普通员工',
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for wechat_subscribe_record
-- ----------------------------
DROP TABLE IF EXISTS `wechat_subscribe_record`;
CREATE TABLE `wechat_subscribe_record` (
  `recordid` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL,
  `dv` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`recordid`)
) ENGINE=InnoDB AUTO_INCREMENT=1618 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- View structure for vclientinfo
-- ----------------------------
DROP VIEW IF EXISTS `vclientinfo`;
CREATE ALGORITHM=UNDEFINED DEFINER=``@`%` SQL SECURITY INVOKER  VIEW `vclientinfo` AS select `c1`.`client_id` AS `client_id`,`c1`.`client_name` AS `client_name`,`c1`.`client_sex` AS `client_sex`,`c1`.`client_phone` AS `client_phone`,`c1`.`client_joindate` AS `client_joindate`,`c1`.`client_birth` AS `client_birth`,`c1`.`client_address` AS `client_address`,`c1`.`client_money` AS `client_money`,`c1`.`client_credit` AS `client_credit`,`c1`.`client_remark` AS `client_remark`,`c1`.`client_storeid` AS `client_storeid`,coalesce(`dc`.`intro_discount`,100) AS `client_discount` from ((`clients` `c1` left join `clients` `c2` on((`c1`.`client_introducer` = `c2`.`client_id`))) left join `introducer_discount` `dc` on((`dc`.`intro_id` = `c2`.`client_id`))) ;

-- ----------------------------
-- View structure for vcompanysimpleinfo
-- ----------------------------
DROP VIEW IF EXISTS `vcompanysimpleinfo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vcompanysimpleinfo` AS select `clients`.`client_id` AS `id`,`clients`.`client_name` AS `code`,`clients`.`client_storeid` AS `store` from `clients` where (`clients`.`client_type` = 0) ;

-- ----------------------------
-- View structure for vproductinfo
-- ----------------------------
DROP VIEW IF EXISTS `vproductinfo`;
CREATE ALGORITHM=UNDEFINED DEFINER=``@`%` SQL SECURITY INVOKER  VIEW `vproductinfo` AS SELECT
	`products_info`.`product_id` AS `product_id`,
	`products_info`.`product_code` AS `product_code`,
	`products_info`.`product_name` AS `product_name`,
	`product_onsale`.`sale_prices` AS `sale_prices`,
	`products_info`.`product_cat` AS `product_cat`,
	`products_info`.`product_readi` AS `product_readi`,
	`product_onsale`.`discount` AS `discount`,
	COALESCE (
		(
			SELECT
				sum(
					`product_in_stock`.`stock_count`
				)
			FROM
				`product_in_stock`
			WHERE
				(
					`product_in_stock`.`product_id` = `products_info`.`product_id`
				)
		),
		0
	) AS `stock_count`,
	COALESCE (
		(
			SELECT
				sum(
					`orders_detail`.`product_count`
				)
			FROM
				`orders_detail`
			WHERE
				(
					`orders_detail`.`product_id` = `products_info`.`product_id` AND 
						`orders_detail`.`is_returned` <> 1
				)
		),
		0
	) AS `sale_count`,
	`products_info`.`product_color` AS `product_color`,
	`products_info`.`product_size` AS `product_size`,
	`product_category`.`cat_name` AS `cat_name`,
	`product_onsale`.`store_id` AS `store_id`,
	`products_info`.`product_start` AS `product_start`,
	(
		SELECT
			`product_images`.`image_path`
		FROM
			`product_images`
		WHERE
			(
				`product_images`.`product_id` = `products_info`.`product_id`
			)
		LIMIT 1
	) AS `catimg`
FROM
	(
		(
			`products_info`
			JOIN `product_onsale`
		)
		LEFT JOIN `product_category` ON (
			(
				`product_category`.`cat_id` = `products_info`.`product_cat`
			)
		)
	)
WHERE
	(
		`products_info`.`product_id` = `product_onsale`.`product_id`
	) ;

-- ----------------------------
-- View structure for vsalehistory
-- ----------------------------
DROP VIEW IF EXISTS `vsalehistory`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vsalehistory` AS select `clients`.`client_wechat_openid` AS `client_wechat_openid`,`salehistory`.`pay_method` AS `pay_method`,`salehistory`.`serial_number` AS `serial_number`,`salehistory`.`order_amount` AS `order_amount`,`salehistory`.`store_id` AS `store_id`,`salehistory`.`order_time` AS `order_time`,`salehistory`.`client_id` AS `client_id`,`salehistory`.`order_id` AS `order_id`,`clients`.`client_name` AS `client_name`,`stores`.`store_name` AS `store_name` from ((`clients` join `salehistory` on((`clients`.`client_id` = `salehistory`.`client_id`))) join `stores` on((`salehistory`.`store_id` = `stores`.`store_id`))) ;

-- ----------------------------
-- View structure for vsalehistorydetails
-- ----------------------------
DROP VIEW IF EXISTS `vsalehistorydetails`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vsalehistorydetails` AS select `salehistory_detail`.`product_id` AS `product_id`,`salehistory_detail`.`product_inprice` AS `product_inprice`,`salehistory_detail`.`product_count` AS `product_count`,`salehistory_detail`.`product_saleprice` AS `product_saleprice`,`salehistory_detail`.`product_discount_price` AS `product_discount_price`,`salehistory_detail`.`order_id` AS `order_id`,`products_info`.`product_name` AS `product_name`,`salehistory_detail`.`detail_id` AS `detail_id`,`salehistory_detail`.`is_returned` AS `is_returned`,`products_info`.`product_code` AS `product_code` from (`salehistory_detail` join `products_info`) where (`products_info`.`product_id` = `salehistory_detail`.`product_id`) ;

-- ----------------------------
-- View structure for vsaletophotmonth
-- ----------------------------
DROP VIEW IF EXISTS `vsaletophotmonth`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vsaletophotmonth` AS select `salehistory_detail`.`product_id` AS `product_id`,`products_info`.`product_name` AS `product_name`,`products_info`.`product_code` AS `product_code`,date_format(`salehistory`.`order_time`,'%Y-%m') AS `month`,sum(`salehistory_detail`.`product_count`) AS `count` from ((`salehistory_detail` join `products_info` on((`salehistory_detail`.`product_id` = `products_info`.`product_id`))) join `salehistory` on((`salehistory_detail`.`order_id` = `salehistory`.`order_id`))) where (`salehistory_detail`.`product_id` <> 7481) group by `products_info`.`product_code`,date_format(`salehistory`.`order_time`,'%Y-%m') order by sum(`salehistory_detail`.`product_count`) desc ;

-- ----------------------------
-- View structure for vsaletophotmonthkh
-- ----------------------------
DROP VIEW IF EXISTS `vsaletophotmonthkh`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vsaletophotmonthkh` AS select left(`products_info`.`product_code`,(length(`products_info`.`product_code`) - 4)) AS `product_code`,`salehistory_detail`.`product_id` AS `product_id`,date_format(`salehistory`.`order_time`,'%Y-%m') AS `month`,sum(`salehistory_detail`.`product_count`) AS `count` from ((`salehistory_detail` join `products_info` on((`salehistory_detail`.`product_id` = `products_info`.`product_id`))) join `salehistory` on((`salehistory_detail`.`order_id` = `salehistory`.`order_id`))) where (`salehistory_detail`.`product_id` <> 7481) group by left(`products_info`.`product_code`,(length(`products_info`.`product_code`) - 4)),date_format(`salehistory`.`order_time`,'%Y-%m') order by sum(`salehistory_detail`.`product_count`) desc ;

-- ----------------------------
-- View structure for vsaletophotweek
-- ----------------------------
DROP VIEW IF EXISTS `vsaletophotweek`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vsaletophotweek` AS select `salehistory_detail`.`product_id` AS `product_id`,`products_info`.`product_name` AS `product_name`,`products_info`.`product_code` AS `product_code`,date_format(`salehistory`.`order_time`,'%Y-%u') AS `week`,sum(`salehistory_detail`.`product_count`) AS `count` from ((`salehistory_detail` join `products_info` on((`salehistory_detail`.`product_id` = `products_info`.`product_id`))) join `salehistory` on((`salehistory_detail`.`order_id` = `salehistory`.`order_id`))) where (`salehistory_detail`.`product_id` <> 7481) group by `products_info`.`product_code`,date_format(`salehistory`.`order_time`,'%u') order by sum(`salehistory_detail`.`product_count`) desc ;

-- ----------------------------
-- View structure for vstatdaysalesum
-- ----------------------------
DROP VIEW IF EXISTS `vstatdaysalesum`;
CREATE ALGORITHM=UNDEFINED DEFINER=``@`%` SQL SECURITY INVOKER  VIEW `vstatdaysalesum` AS SELECT
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	) AS `day`,
	sum(
		`sd`.`product_discount_price`
	) AS `sum`,
	sum(`sd`.`product_count`) AS `count`,
	sum(
		(
			`sd`.`product_discount_price` - `sd`.`product_inprice`
		)
	) AS `profits`,
	`sh`.`pay_method` AS `pay_method`,
	`sh`.`store_id` AS `store_id`
FROM
	(
		`orders_detail` `sd`
		JOIN `orders` `sh` ON (
			(
				(
					`sd`.`order_id` = `sh`.`order_id`
				)
				AND (`sd`.`is_returned` <> 1)
			)
		)
	)
GROUP BY
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	),
	`sh`.`pay_method`
ORDER BY
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	) DESC ;

-- ----------------------------
-- View structure for vstatdaysalesumraw
-- ----------------------------
DROP VIEW IF EXISTS `vstatdaysalesumraw`;
CREATE ALGORITHM=UNDEFINED DEFINER=``@`%` SQL SECURITY INVOKER  VIEW `vstatdaysalesumraw` AS SELECT
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	) AS `day`,
	sum(
		`sd`.`product_discount_price` * `sd`.`product_count`
	) AS `sum`,
	sum(`sd`.`product_count`) AS `count`,
	sum(
		(
			`sd`.`product_discount_price` - `sd`.`product_inprice`
		)
	) AS `profits`,
	`sh`.`store_id` AS `store_id`
FROM
	(
		`orders_detail` `sd`
		JOIN `orders` `sh` ON (
			(
				(
					`sd`.`order_id` = `sh`.`order_id`
				)
				AND (`sd`.`is_returned` <> 1)
			)
		)
	)
GROUP BY
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	)
ORDER BY
	date_format(
		`sh`.`order_time`,
		'%Y-%m-%d'
	) DESC ;

-- ----------------------------
-- View structure for vstatmonthsalesum
-- ----------------------------
DROP VIEW IF EXISTS `vstatmonthsalesum`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hamer`@`%` SQL SECURITY DEFINER  VIEW `vstatmonthsalesum` AS select date_format(`sh`.`order_time`,'%Y-%m') AS `month`,sum(`sh`.`order_amount`) AS `sum`,sum((select sum(`salehistory_detail`.`product_count`) from `salehistory_detail` where ((`salehistory_detail`.`order_id` = `sh`.`order_id`) and (`salehistory_detail`.`is_returned` <> 1)))) AS `count`,sum((select sum((`salehistory_detail`.`product_discount_price` - `salehistory_detail`.`product_inprice`)) from `salehistory_detail` where ((`salehistory_detail`.`order_id` = `sh`.`order_id`) and (`salehistory_detail`.`is_returned` <> 1)))) AS `profits`,`sh`.`pay_method` AS `pay_method`,`sh`.`store_id` AS `store_id` from `salehistory` `sh` group by date_format(`sh`.`order_time`,'%m'),`sh`.`pay_method` ;

-- ----------------------------
-- View structure for vstatmonthsalesumraw
-- ----------------------------
DROP VIEW IF EXISTS `vstatmonthsalesumraw`;
CREATE ALGORITHM=UNDEFINED DEFINER=``@`%` SQL SECURITY INVOKER  VIEW `vstatmonthsalesumraw` AS SELECT
	date_format(`sh`.`order_time`, '%Y-%m') AS `month`,
	sum(`sh`.`order_amount`) AS `sum`,
	sum(
		(
			SELECT
				sum(
					`orders_detail`.`product_count`
				)
			FROM
				`orders_detail`
			WHERE
				(
					(
						`orders_detail`.`order_id` = `sh`.`order_id`
					)
					AND (
						`orders_detail`.`is_returned` <> 1
					)
				)
		)
	) AS `count`,
	sum(
		(
			SELECT
				sum(
					(
						`orders_detail`.`product_discount_price` - `orders_detail`.`product_inprice`
					)
				)
			FROM
				`orders_detail`
			WHERE
				(
					(
						`orders_detail`.`order_id` = `sh`.`order_id`
					)
					AND (
						`orders_detail`.`is_returned` <> 1
					)
				)
		)
	) AS `profits`,
	`sh`.`store_id` AS `store_id`
FROM
	`orders` `sh`
GROUP BY
	date_format(`sh`.`order_time`, '%m') ;
DROP TRIGGER IF EXISTS `company_income`;
DELIMITER ;;
CREATE TRIGGER `company_income` AFTER INSERT ON `company_income_record` FOR EACH ROW UPDATE `clients` SET `client_money` = `client_money` + NEW.amount WHERE `clients`.client_wechat_openid = NEW.com_id
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `salehistory_liucode_trigger`;
DELIMITER ;;
CREATE TRIGGER `salehistory_liucode_trigger` BEFORE INSERT ON `orders` FOR EACH ROW BEGIN
	SET NEW.serial_number = NOW() + 0;
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `salehistory_credit_trigger`;
DELIMITER ;;
CREATE TRIGGER `salehistory_credit_trigger` AFTER INSERT ON `orders` FOR EACH ROW BEGIN 
-- 积分变动量
SET @AMOUNT = TRUNCATE((NEW.order_amount+5)/10, 0);
-- 更新积分项
UPDATE clients SET client_credit = client_credit + @AMOUNT WHERE client_id = NEW.client_id;
-- 如果是会员 加积分 10比1
IF(NEW.client_id > 0) THEN
INSERT INTO client_credit_record (client_id,amount,`time`,staff_id,store_id) VALUES (NEW.client_id,@AMOUNT,NOW(),NEW.staff_id,NEW.store_id);
END IF;

-- 商家返现
SET @INTRO = (SELECT c2.client_id FROM clients c1 JOIN clients c2 ON c2.client_id = c1.client_introducer WHERE c1.client_id = NEW.client_id AND c2.client_type = 0);
if(NOT ISNULL(@INTRO)) THEN
UPDATE clients SET client_money = client_money + (NEW.order_amount * 0.1) WHERE client_id = @INTRO;
END IF;

END
;;
DELIMITER ;
