<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/css/usc.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body>
        <div class="uc-headwrap" style='background-image: url(static/images/ucbag/bag{$bagRand}.jpg);'>
            <div class="uc-head">
                <a class="headwrap"><img src="{$userinfo.uhead}/132" /></a>
                <span class="uc-name">{$userinfo.nickname}</span>
                <span class="uc-addr">{$userinfo.address}</span>
            </div>
        </div>

        <!-- home nav -->
        <div class="uc-section" data-swclass="uc-section-hover" data-link="?/Uc/companySpread">推广赚钱</div>
        <div class="uc-section" data-swclass="uc-section-hover" data-link="?/Uc/orderlist">全部订单</span></div>
        <div class="uc-section" data-swclass="uc-section-hover" data-link="?/Uc/orderlist/status=delivering">待收货订单<span class="uc-section-count">{$devliCount}</span></div>
        <div class="uc-section" data-swclass="uc-section-hover" data-link="wxpay.php">购物车<span class="uc-section-count" id="cart-count"></div>
        <div class="uc-section" data-swclass="uc-section-hover" data-link="?/">返回首页</div>
        <!-- home nav -->

        {include file="../global/footer.tpl"}
        <script type="text/javascript">
            WeixinJSBridgeReady(UchomeLoad);
        </script>
    </body>
</html>