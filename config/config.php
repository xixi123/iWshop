<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

$dirname = dirname(__FILE__);

include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'sys_config.php';

/**
 * 路径分割 win下\ linux下/
 */
define(DIRSEP, DIRECTORY_SEPARATOR);

/**
 * 微信开发者APPID
 * Wechat Application Id
 */
define("APPID", "wx912921749b35d0f3");

/**
 * 微信开发者APPSECRET
 * Wechat Application Secret
 */
define("APPSECRET", "19cf050b78cb293bf375f638053a4d38");

/**
 * 微信开发者验证TOKEN
 * Wechat Token
 */
define(TOKEN, "xiaoxiao");

/**
 * <财付通> 商户ID(partnerId)
 * @see 请查阅商户开通邮件
 */
define(PARTNER, "1219320201");

/**
 * <财付通> 商户支付密钥(paySign key)
 * @see 请查阅商户开通邮件
 */
define(APPKEY ,"FDKsNQqgt6dquZFaG81UCdGfrItzKOAgkPCIYrOjf9AbV6l9WLzH5WpJyZLVGvlHzHboYOob3tiodrnmeUpUo9sKy7X7GqBPlZ0aAPyonqdpOEhn4Zuyjrl2KFfaWX3C"); 

/**
 * <财付通> 商户通加密串(partnerKey)
 * @see 请查阅商户开通邮件
 */
define("PARTNERKEY","27f5c84489ca6e7f5e0cdaa6f6252e23");

/**
 * 微信签名方式:sha1
 * SIGNTYPE
 */
define("SIGNTYPE", "sha1");

/**
 * weimi短信接口
 */
define('WEIMIUID','SqTOhc86HEuU');
define('WEIMIKEY','z7afpr9n');

/**
 * weimi短信模板
 */
define('WEIMITEMPLATE1', 'dgYWkeCxx6cnull');

/**
 * Conifg Object
 */
$config = new stdClass();

/**
 * 系统初始化自动加载模块
 */
$config->preload = array(
      'Controller'   // 系统控制器
    , 'Model'        // 系统模型超类
    , 'Smarty.class' // Smarty模板引擎
    , 'Db'           // 数据库连接
);

/**
 * 模块加载自动查找路径
 */
$config->classRoot = array(
    $dirname . "/../controllers/",
    $dirname . "/../models/",
    $dirname . "/../system/",
    $dirname . '/../lib/',
    $dirname . '/../lib/Smarty/',
    $dirname . '/../lib/Smarty/plugins/',
    $dirname . '/../lib/Smarty/sysplugins/',
    $dirname . '/../lib/barcodegen/',
    $dirname . '/../lib/phpqrcode/'
);

/**
 * 控制器默认方法，最终默认为index
 */
$config->defaultAction = array(
    'ViewProduct' => 'view_list'
);

/**
 * 默认视图文件目录
 */
$config->tpl_dir = "views";

/**
 * 默认Controller
 */
$config->default_controller = "Index";

/**
 * Debug开关
 */
$config->debug = true;

/**
 * Db configuration
 */
$config->db = array(
    'host' => 'localhost',
    'db' => 'wshop',
    'user' => 'hammer',
    'pass' => 'a919161A'
);

// GC
unset($dirname);