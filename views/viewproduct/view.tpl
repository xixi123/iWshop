<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/css/common.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body>

        <input type="hidden" value="{$comid}" id="comid" />
        <input type="hidden" value="{$productInfo.product_name}" id="sharetitle" />
        <input type="hidden" value="{$productid}" id="iproductId" />
        <input type="hidden" value="{$shareimg}" id="shareimg" />

        <div class="listTopcaption">
            <div class="listTopArrow" data-swclass="arraw-ani" data-link="javascript:history.go(-1);"></div>
            <a class="listTopArrow home" href="javascript:;" onclick="location = '?/';"></a>
            {$productInfo.product_name}
        </div>

        <div id="slider" {if $vproductheight}style="height:{$vproductheight}px"{/if}>
            {if $showSbar}
                <div class="sliderTip">
                    {section name=ii loop=$images}
                        <i class="sliderTipItems {if $smarty.section.ii.index == 0}current{/if}"></i>
                    {/section}
                </div>
            {/if}
            {strip}
                {section name=ii loop=$images}
                    <div class="slider" style="{if $vproductheight}height:{$vproductheight}px;{/if}background-image:url('static/product_hpic/{$images[ii].image_path}');"></div>
                {/section}
            {/strip}
        </div>

        <div id="container">
            <article class="title">{$productInfo.product_name}</article>
            <span class="show"><span class="captip">售价：</span>&yen;{$productInfo.sale_prices}</span>
            <div style='margin-top: 8px;'>
                <span style="color:#000;display: inline-block;float:left;height: 25px;line-height: 25px;">数量：</span>
                <div class="productCount clearfix">
                    <a class="btn" id="productCountMinus" href='javascript:;'></a>
                    <span id="productCountNum"><input type='tel' value='1' id="productCountNumi" /></span>
                    <a class="btn" id="productCountPlus" href='javascript:;'></a>
                </div>
            </div>
        </div>
        <div id="buybutton1" class="clearfix">
            <a class="button" id="addcart-button" href="javascript:;" onclick="addToCart();">加入购物车</a>
            <a class="button" id="buy-button" href="javascript:;" onclick="addToCart();
                    location = 'wxpay.php';">立即购买</a>
        </div>
        <div id="content">{$productInfo2.product_desc}</div>
        <script type="text/javascript">
            WeixinJSBridgeReady(viewproductOnload);
        </script>
        {include file="../global/footer.tpl"}
    </body>
</html>