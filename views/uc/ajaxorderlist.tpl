{section name=oi loop=$orders}
    <div class="uc-orderitem" id="orderitem{$orders[oi].order_id}">
        <div class="uc-seral clearfix">
            <p class="order_serial">订单号：{$orders[oi].serial_number}</p>
            <p class="order_time">{$orders[oi].order_time}</p>
        </div>
        {section name=di loop=$orders[oi]['data']}
            <div class="clearfix items" onclick="location = '?/ViewProduct/view/id={$orders[oi]['data'][di].product_id}/showwxpaytitle=1';">
                <img class="ucoi-pic" src="static/product_hpic/{$orders[oi]['data'][di].catimg}">
                <div class="ucoi-con">
                    <span class="title">{$orders[oi]['data'][di].product_name}</span>
                    <span class="price">&yen;<span class="dprice">{$orders[oi]['data'][di].product_discount_price}</span> x <span class="dcount">{$orders[oi]['data'][di].product_count}</span></span>
                </div>
            </div>
        {/section}
        <div class="uc-summary clearfix">
            <span class="sum">&yen;<b>{$orders[oi].order_amount}</b></span>
            {if $orders[oi].status == "unpay"}
                <a class="olbtn cancel" href="javascript:;" onclick="Orders.cancelOrder({$orders[oi].order_id},this);">取消订单</a>
                <a class="olbtn wepay" href="javascript:confirmExpress({$orders[oi].order_id});">微信支付</a>
            {else if $orders[oi].status == "payed"}
                <a class="olbtn cancel" href="javascript:;" onclick="Orders.cancelOrder({$orders[oi].order_id},this);">取消订单</a>
            {else if $orders[oi].status == "delivering"}
                <a class="olbtn comfirm" href="javascript:Orders.confirmExpress({$orders[oi].order_id});">确认收货</a>
                <a class="olbtn express" href="?/Order/expressDetail/order_id={$orders[oi].order_id}">查看物流</a>
            {else if $orders[oi].status == "received"}
                <a class="olbtn comment" href="javascript:confirmExpress({$orders[oi].order_id});">评价一下</a>
            {/if}
        </div>
    </div>
{/section}