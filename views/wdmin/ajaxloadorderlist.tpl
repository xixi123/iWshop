{section name=oi loop=$orderlist}
    <div class="orderlist" data-id="{$orderlist[oi].order_id}" data-pid="{$orderlist[oi].wepay_serial}" id="orderlst{$orderlist[oi].order_id}">
        <div id="orderWpa{$orderlist[oi].order_id}">
            <div class="orderwpa-top">
                <span class="orderwpa-serial">
                    订单号: {$orderlist[oi].serial_number} 
                    支付号: {$orderlist[oi].wepay_serial}
                </span>
                <span class="orderwpa-amount">&yen;{$orderlist[oi].order_amount}</span>
            </div>
            {section name=od loop=$orderlist[oi].data}
                <div style="margin-top: 10px;">
                    <img width="40px" style="float:left;border:1px solid #ddd;" src="static/product_hpic/{$orderlist[oi].data[od].catimg}" />
                    <div style="margin-left: 50px;">
                        {$orderlist[oi].data[od].product_name}
                        <br />
                        <i class="opprice">&yen;{$orderlist[oi].data[od].product_discount_price}</i> &times; 
                        <i id="order{$orderlist[oi].order_id}count" class="opcount">{$orderlist[oi].data[od].product_count}件</i>
                    </div>
                </div>
            {/section}
            <div style="padding: 5px 0;text-align: left;border-top:1px solid #dedede;font-size:13px;color:#666;line-height:25px;margin-top: 13px;" class="clearfix">
                <span style='font-size: 14px;color:#222;'>{$orderlist[oi].address.user_name}</span> 电话：{$orderlist[oi].address.tel_number} 邮编：{$orderlist[oi].address.postal_code} <br/>
                地址：{$orderlist[oi].address.address}
            </div>
        </div>
        <div class="orderwpa-bottom clearfix">
            <a class="olbtn express various" href="#inline" data-orderid="{$orderlist[oi].order_id}">发货通知</a>
        </div>
    </div>
{/section}