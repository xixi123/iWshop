<?php

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * System Super Class Model
 */
class Model {

    private $Controller;

    // 构造方法
    public function __construct() {
        
    }

    function __get($name) {
        $class = $this->Controller;
        if (property_exists($class, $name)) {
            return $this->Controller->$name;
        }
    }

    /**
     * magic call
     * @param type $name
     * @param type $arguments
     */
    function __call($name, $arguments) {
        // 对Controller进行动态反射，跨对象调用
        $class = new ReflectionClass('Controller');
        try {
            $ec = $class->getMethod($name);
            return $ec->invokeArgs($this->Controller, $arguments);
        } catch (ReflectionException $re) {
            die('Fatal Error : ' . $re->getMessage());
        }
        return false;
    }

    /**
     * hook
     * 手动挂载model到另外一个model
     * @todo 自动挂载
     */
    public function hook($classA = array()) {
        foreach ($classA AS $class) {
            $className = get_class($class);
            $this->$className = $class;
            unset($className);
        }
    }

    public function linkController($obj) {
        $this->Controller = $obj;
    }

}
