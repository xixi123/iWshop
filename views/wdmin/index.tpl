<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>微点客户管理后台</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <link href="https://res.wx.qq.com/mpres/htmledition/images/favicon1e5b3a.ico" rel="Shortcut Icon" />
        <link href="static/css/wadmin.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/script/fancyBox/source/jquery.fancybox.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="static/script/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="static/script/highcharts/js/modules/exporting.js"></script>
        <script type="text/javascript" src="static/script/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/wdmin.js?v={$cssversion}"></script>
    </head>
    <body class="wdmin-main">
        <div id="leftNav">
            {*<a class="navItem" id="navitem1"></a>*}
            <a class="navItem" id="navitem2" onclick="javascript:switchCard(1);" href="#report">报表中心</a>
            {*<a class="navItem" id="navitem3">用户管理</a>*}
            <a class="navItem" id="navitem4" onclick="javascript:switchCard(2);" href="#orders">
                <i id="stat-ordercount" class="nav-count">0</i>
                订单发货
            </a>
        </div>
        <div id="rightWrapper">
            <div id="main-top">
                <div id="wes-bar">
                    <a class="barItem">
                        <img class="usrhead" src="static/images/admin/logo.jpg"/>
                    </a>
                    <a class="barItem pd7">
                        总关注 {$wechatSub} 人
                    </a>                  
                    {*                    <a class="barItem pd7">
                    新增关注 1 人
                    </a>                  
                    <a class="barItem pd7">
                    取消关注 1 人
                    </a>*}                  
                </div>{*
                <div id="usr-bar">
                    <a class="barItem pd7 saledata">
                        今日销售额 <b>&yen;{if $daysale.sum}{$daysale.sum}{else}0{/if}</b>
                    </a>       
                    <a class="barItem pd7 saledata">
                        本月销售额 <b>&yen;{if $monthsale.sum}{$monthsale.sum}{else}0{/if}</b>
                    </a>       
                </div>*}
            </div>
            <div id="main-mid">
                <div class="_card" id="card1">
                    <div class="stat-item-head">
                        <span class="stat-item-1">今日浏览量：<b id="daytot"></b></span>
                        <span class="stat-item-1">本月浏览量：<b id="montot"></b></span>
                    </div>
                    <div id="pageviewchart" style="height:200px"></div>
                    <div class="stat-item-head">
                        <span class="stat-item-1">今日销售额 <b>&yen;{if $daysale.sum}{$daysale.sum}{else}0{/if}</b></span>
                        <span class="stat-item-1">本月销售额 <b>&yen;{if $monthsale.sum}{$monthsale.sum}{else}0{/if}</b></span>
                    </div>
                    <div id="saletReachart" style="height:200px"></div>
                </div>
                <div class="_card show" id="card2" style="background: #eee;">
                    <div id="orderlist"></div>
                </div>
            </div>
        </div>
        <div id="inline" style="display:none;width:500px;">
            <div id="_orderwpa"></div>
            <div style="text-align: center;border-top: 1px solid #dedede;padding-top: 5px;">
                <input type="text" id='despatchExpressCode' value="" placeholder="请填写快递单号"/>
                <select id="expressCompany">
                    {foreach from=$expressCompany key=myId item=i}
                        <option value="{$myId}">{$i}</option>
                    {/foreach}
                </select>
            </div>
            <div style="text-align: center;margin-top: 7px;">
                <a class="olbtn" id='despatchBtn' href='javascript:;'>确认</a>
            </div>
        </div>
    </body>
</html>