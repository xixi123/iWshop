<?php

/*
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

// 推广记录表
define('COMPANY_SPREAD', '`company_spread_record`');

// 订单表
define('TABLE_ORDERS', 'orders');

// 订单详情表
define('TABLE_ORDERS_DETAIL', 'orders_detail');

define('TABLE_ORDERS_ADDRESS', 'orders_address');

define('TABLE_WDMIN_LOGIN_TOKEN', '`admin_login_code_token`');

define('DEVREPORT_OPENID','od2ZEuGcj5xSuk_YJuSpJ2wPsKp0');