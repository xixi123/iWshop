<?php

/*
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

class ViewProduct extends Controller {

    /**
     * 商品列表显示数量
     */
    const LIST_LIMIT = 10;

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('Db');
    }

    /**
     * Product Detail
     * @param type $Query
     */
    public function view($Query) {
        // 推荐com，90分钟
        if (!isset($_COOKIE['com']) && isset($Query->com)) {
            setcookie("com", $Query->com, time() + 5400);
        }

        if (isset($Query->com)) {
            // spread++
            $spreadUpdateSql = "UPDATE " . COMPANY_SPREAD . " SET `readi` = `readi` + 1 WHERE `product_id` = $Query->id AND `com_id` = '$Query->com';";
            $this->Db->query($spreadUpdateSql);
        }
        
        $openid = $this->getOpenId();

        $Query->id = intval($Query->id);
        $productInfo = $this->Db->query(sprintf("SELECT * FROM `vProductInfo` WHERE product_id = %s;", $Query->id));
        $productInfo = $productInfo[0];
        $productInfo2 = $this->Db->query(sprintf("SELECT * FROM `products_info` WHERE product_id = %s;", $Query->id));
        $productInfo2 = $productInfo2[0];

        // readi
        $this->Db->query("UPDATE `products_info` SET `product_readi` = `product_readi` + 1 WHERE `product_id` = $Query->id;");
        // product images
        $images = $this->Db->query("SELECT * FROM `product_images` WHERE `product_id` = $Query->id;");

        $this->Smarty->assign('shareimg', 'http://112.124.44.172/wshop/static/product_hpic/' . $images[0]['image_path']);
        $this->Smarty->assign('vproductheight', isset($_COOKIE['vproductheight']) ? $_COOKIE['vproductheight'] : false);
        $this->Smarty->assign('showSbar', count($images) > 1);
        $this->Smarty->assign('images', $images);
        $this->Smarty->assign('productInfo', $productInfo);
        $this->Smarty->assign('productInfo2', $productInfo2);
        $this->Smarty->assign('productid', $Query->id);
        $this->Smarty->assign('comid', $openid);
        $this->show();
    }

    /**
     * Product list
     * @param type $Query
     */
    public function view_list($Query) {
        $this->getOpenId();
        
        !isset($Query->page) && $Query->page = 0;
        !isset($Query->searchkey) && $Query->searchkey = '';
        !isset($Query->cat) && $Query->cat = 1;
        !isset($Query->orderby) && $Query->orderby = "";
        $Query->searchkey = urldecode($Query->searchkey);

        // 推荐com，90分钟
        if (!isset($_COOKIE['com']) && isset($Query->com)) {
            setcookie("com", $Query->com, time() + 5400);
        }

        // params
        if ($Query->searchkey != '') {
            $catInfo = array(
                'cat_id' => $Query->cat,
                'cat_name' => $Query->searchkey . '的搜索结果'
            );
        } else {
            $this->loadModel('Product');
            $this->Product->hook(array($this->Db));
            $catInfo = $this->Product->getCatInfo($Query->cat);
        }

        $this->Smarty->assign('query', $Query);
        $this->Smarty->assign('searchkey', $Query->searchkey);
        $this->Smarty->assign('cat', $Query->cat);
        $this->Smarty->assign('catInfo', $catInfo);
        $this->Smarty->assign('orderby', $Query->orderby);
        $this->show();
    }

    /**
     * Ajax返回商品列表
     * @todo cat
     * @param type $Query
     */
    public function ajaxProductList($Query) {
        // page
        !isset($Query->page) && $Query->page = 0;
        // order by
        if (!isset($Query->orderby) || $Query->orderby == "") {
            $Query->orderby = '`product_start` DESC';
        } else {
            $Query->orderby = trim(urldecode($Query->orderby));
        }
        // product category
        !isset($Query->cat) && $Query->cat = 1;
        // Query limit statment
        $_SQL_limit = $Query->page * self::LIST_LIMIT . "," . self::LIST_LIMIT;
        // search key
        if (isset($Query->searchKey) && $Query->searchKey != '') {
            $Query->searchKey = urldecode($Query->searchKey);
            $_SQL = sprintf("SELECT * FROM `vProductInfo` WHERE `product_name` LIKE '%%$Query->searchKey%%' ORDER BY %s LIMIT %s;", $Query->orderby, $_SQL_limit);
        } else {
            $_SQL = sprintf("SELECT * FROM `vProductInfo` WHERE `product_cat` = $Query->cat ORDER BY %s LIMIT %s;", $Query->orderby, $_SQL_limit);
        }
        $productList = $this->Db->query($_SQL);
        if (count($productList) == 0) {
            die('0');
        } else {
            $this->Smarty->assign('product_list', $this->Db->query($_SQL));
            $this->show('global/product_list.tpl');
        }
    }

    /**
     * Cart data
     * @return HTML STRING
     */
    public function cartData() {
        $data = json_decode($_POST['data'], true);
        $_tmp = array();
        foreach ($data as $key => $product) {
            array_push($_tmp, $key);
        }
        $_SQL = sprintf("SELECT * FROM `vProductInfo` WHERE `product_id` IN (%s);", implode(',', $_tmp));
        $this->Smarty->assign('product_list', $this->Db->query($_SQL));
        $this->Smarty->assign('product_data', $data);
        $this->show();
    }

}
