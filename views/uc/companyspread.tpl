<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/css/usc.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body>
        <input type="hidden" value="{$status}" id="status" />
        <div class="listTopcaption">
            <div class="listTopArrow" data-swclass="arraw-ani" data-link="javascript:history.go(-1);"></div>
            <a class="listTopArrow home" href="javascript:;" onclick="location = '?/';"></a>
            我的推广
        </div>       
       
        <div class="comspreadstat clearfix">
            <span class="spread-item">今日: &yen;{$stat_data['incometot']}</span>
            <span class="spread-item">总计: &yen;{$stat_data['incometot']}</span>
            <span class="spread-item">点击: {if $stat_data['readi']}{$stat_data['readi']}{else}0{/if}</span>
            <span class="spread-item">转化: {if $stat_data['turnrate']}{$stat_data['turnrate']}{else}0{/if}%</span>
        </div>
        
        <div id="uc-spreadlist"></div>
        <div id="loading-wrap"></div>
        
        {include file="../global/footer.tpl"}
        <script type="text/javascript">
            WeixinJSBridgeReady(spreadListOnload);
        </script>
    </body>
</html>