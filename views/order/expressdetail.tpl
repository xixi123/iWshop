<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/css/usc.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body>

        <input type="hidden" value="{$orderdetail.express_code}" id="expresscode" />
        <input type="hidden" value="{$orderdetail.express_com}" id="expresscom" />

        <div class="listTopcaption">
            <a class="listTopArrow" href="javascript:;" onclick="history.go(-1);"></a>
            <a class="listTopArrow home" href="javascript:;" onclick="location = '?/';"></a>
            查看物流
        </div>

        <div class="exp-head">
            <div id="exp-comname">{$orderdetail.express_com1}</div>
            <div id="exp-code">运单编号：{$orderdetail.express_code}</div>
        </div>

        <div class="exp-item-info">
            <div class="exp-item-caption">物流跟踪</div>
            <ul id="express-dt"></ul>
            <div id="loading-wrap"></div>
        </div>

        <div class="exp-item-info">
            <div class="exp-item-caption">物品信息</div>
            {section name=pi loop=$productlist}
                <div class="clearfix items" onclick="location = '?/ViewProduct/view/id={$productlist[pi].product_id}/showwxpaytitle=1';">
                    <img class="ucoi-pic" src="static/product_hpic/{$productlist[pi].catimg}">
                    <div class="ucoi-con">
                        <span class="title">{$productlist[pi].product_name}</span>
                        <span class="price"><span class="dcount">{$productlist[pi].product_count}</span> 件</span>
                    </div>
                </div>
            {/section}
            {if $orderdetail.status == "delivering"}
                <div id="expressapi-cop"><a id="express-confirm" href="javascript:Orders.confirmExpress({$orderdetail.order_id});">确认收货</a></div>
            {/if}
        </div>

        <script type="text/javascript">
            WeixinJSBridgeReady(ExpressDetailOnload);
        </script>

        {include file="../global/footer.tpl"}
    </body>
</html>