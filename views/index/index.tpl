<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>熹贝母婴用品</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="static/script/main.js"></script>
    </head>
    <body>

        <header class="index-header">
            <div id="topbanner"></div>
            <div id="slider">
                <div class="sliderTip">
                    <i class="sliderTipItems current"></i>
                    <i class="sliderTipItems "></i>
                    <i class="sliderTipItems "></i>
                    <i class="sliderTipItems "></i>
                    <i class="sliderTipItems "></i>
                </div>
                {strip}
                    <div class="slider" onclick="location = '?/ViewProduct/view_list/cat=1&searchkey=德国喜宝Hipp';" style="background-image:url('static/images/slider/bgn4.jpg');"></div>
                    <div class="slider" onclick="location = '?/ViewProduct/view_list/cat=2&searchkey=Karicare';" style="background-image:url('static/images/slider/bgn1.jpg');"></div>
                    <div class="slider" onclick="location = '?/ViewProduct/view_list/cat=1&searchkey=泡泡虫';" style="background-image:url('static/images/slider/bgn2.jpg');"></div>
                    <div class="slider" onclick="location = '?/ViewProduct/view_list/cat=1&searchkey=Friso';" style="background-image:url('static/images/slider/bgn3.jpg');"></div>
                    <div class="slider" onclick="location = '?/ViewProduct/view_list/cat=1&searchkey=大王';" style="background-image:url('static/images/slider/bgn5.jpg');"></div>
                {/strip}
            </div>
        </header>

        {include file="../global/search_box1.tpl"}


        {*        <div style="padding:10px;">
        <section class="navbox" id="home-nav">
        {section name=ci loop=$catList}<section onclick="location = '?/ViewProduct/view_list/cat={$catList[ci].cat_id}';"><a class="nav-round" style="background-image:url({$catList[ci].cat_image});"></a><span>{$catList[ci].cat_name}</span></section>{/section}
        </section>
        </div>*}

        <div id="home-nav-h">
            {section name=ci loop=$catList}
                <div class="nav-btn hftcat{$catList[ci].cat_id}" data-swclass="uc-section-hover" data-link="?/ViewProduct/view_list/cat={$catList[ci].cat_id}">{$catList[ci].cat_name}</div>
            {/section}
        </div>

        {* <div class="font-miaos">
            
        <div class="font-miaos-caption">
        掌上秒杀<span id="fms-tmine-hour">01</span>:<span id="fms-tmine-min">23</span>:<span id="fms-tmine-sec">56</span>
        </div>
        
        <div class="fps-title clearfix">
        掌上秒杀
        <a class="more" href="javascript:;" onclick="location = '?/ViewProduct/view_list/orderby=`sale_count`';">更多</a>
        </div>
        <section class="navbox">
        <section class='font-miaos-box' onclick="location = '?/ViewProduct/view';" style='margin-right: 0;'>
        <img class='miaos-boximage' src='static/images/rBEhVVK7j8YIAAAAAAFxX32fQA0AAHXVQCDCO4AAXF3948.jpg' />
        <span><span class='miaos-cat'>¥ 469.00</span></span>
        <span><span class='miaos-cat-dis'>6.2折</span></span>
        </section>
        <section class='font-miaos-box' onclick="location = '?/ViewProduct/view';" style='margin-right: 0;'>
        <img class='miaos-boximage' src='static/images/rBEhVVK7j8YIAAAAAAFxX32fQA0AAHXVQCDCO4AAXF3948.jpg' />
        <span><span class='miaos-cat'>¥ 469.00</span></span>
        <span><span class='miaos-cat-dis'>5.2折</span></span>
        </section>
        <section class='font-miaos-box' onclick="location = '?/ViewProduct/view';">
        <img class='miaos-boximage' src='static/images/rBEhVVK7j8YIAAAAAAFxX32fQA0AAHXVQCDCO4AAXF3948.jpg' />
        <span><span class='miaos-cat'>¥ 469.00</span></span>
        <span><span class='miaos-cat-dis'>7.1折</span></span>
        </section>
        </section>
        </div>*}

        <div class="font-product-sec" style="margin-top:0;">
            <div class="fps-title clearfix" onclick="location = '?/ViewProduct/view_list/orderby=`sale_count`';">
                热卖商品
            </div>
            <section class="fbox">
                {section name=ph loop=$productHot}<section class='font-miaos-box' data-swclass="box-scale" data-link="?/ViewProduct/view/id={$productHot[ph].product_id}&showwxpaytitle=1"><img class='miaos-boximage' src='static/product_hpic/{$productHot[ph].catimg}' /><i class="psi">&yen;{$productHot[ph].sale_prices}</i></section>{/section}
            </section>
        </div>
        <div class="font-product-sec">
            <div class="fps-title clearfix" onclick="location = '?/ViewProduct/view_list';">
                最新上市
            </div>
            <section class="fbox">
                {section name=pn loop=$productNew}<section class='font-miaos-box' data-swclass="box-scale" data-link="?/ViewProduct/view/id={$productNew[pn].product_id}&showwxpaytitle=1"><img class='miaos-boximage' src='static/product_hpic/{$productNew[pn].catimg}' /><i class="psi">&yen;{$productNew[pn].sale_prices}</i></section>{/section}
            </section>
        </div>
        <script type="text/javascript">
            WeixinJSBridgeReady(homeOnload);
        </script>
        {include file="../global/footer.tpl"}
    </body>
</html>