<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>&nbsp;</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="static/css/StyleSheet.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <link href="static/css/usc.css?v={$cssversion}" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="static/script/jquery-1.9.1.min.js?v={$cssversion}"></script>
        <script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
    </head>
    <body>
        <input type="hidden" value="{$status}" id="status" />
        <div class="listTopcaption">
            <a class="listTopArrow" href="javascript:;" onclick="history.go(-1);"></a>
            <a class="listTopArrow home" href="javascript:;" onclick="location = '?/';"></a>
            全部订单
        </div>
        <div id="uc-orderlist"></div>
        <div id="loading-wrap"></div>
        {include file="../global/footer.tpl"}
    </body>
</html>